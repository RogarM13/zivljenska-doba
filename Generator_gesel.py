__author__ = 'Gregor, Matic'
from tkinter import *

from tip import *


class Geslar():
    def __init__(self, master):

        self.napaka = Label(master, text="Prosim vnesite samo številke", fg="red")
        # možnost 1
        self.moz1 = Frame(master)

        skupi = Frame(self.moz1)
        skupi.pack()
        # Izbor jezika
        self.jez = LabelFrame(skupi, text="Jezik besed")
        self.jez.pack(side="left", anchor="n")
        self.jezik = StringVar()
        self.jezik.set("0")

        jezik0 = Radiobutton(self.jez, text="Slo", variable=self.jezik, value="0")
        jezik0.pack(anchor="w")
        jezik1 = Radiobutton(self.jez, text="Ang", variable=self.jezik, value="1")
        jezik1.pack(anchor="w")
        jezik2 = Radiobutton(self.jez, text="Nem", variable=self.jezik, value="2")
        jezik2.pack(anchor="w")

        self.dolzina = StringVar()
        self.dolzina.set("8")
        self.dolzi = LabelFrame(self.moz1, text="Dolžina gesla (6-20)")
        self.dolzi.pack(anchor="n", side="top")

        dolz = Entry(self.dolzi, textvariable=self.dolzina, justify=CENTER)
        dolz.pack()
        dolz.bind("<Return>", self.znano)

        self.crk = StringVar()
        self.crk.set("0")
        self.cr = LabelFrame(skupi, text="Velike/male črke")
        self.cr.pack(side="left", anchor="n")

        # izbor velike male
        cr0 = Radiobutton(self.cr, text="Samo male", variable=self.crk, value=0)
        cr0.pack(anchor="w")
        cr1 = Radiobutton(self.cr, text="Samo velike", variable=self.crk, value=1)
        cr1.pack(anchor="w")
        cr2 = Radiobutton(self.cr, text="Ena mala", variable=self.crk, value=2)
        cr2.pack(anchor="w")
        cr3 = Radiobutton(self.cr, text="Ena velika", variable=self.crk, value=3)
        cr3.pack(anchor="w")
        cr4 = Radiobutton(self.cr, text="Mešano", variable=self.crk, value=4)
        cr4.pack(anchor="w")


        # Dodatni znaki

        self.zna = LabelFrame(skupi, text="Dovoljeni znaki")
        self.zna.pack()
        self.zna1 = Frame(self.zna)
        self.zna2 = Frame(self.zna)
        self.zna3 = Frame(self.zna)
        self.zna1.pack(side="left")
        self.zna3.pack(side="right")
        self.zna2.pack(side="right")

        self.znaki = StringVar("")


        self.a = StringVar("")
        a = Checkbutton(self.zna1, text="!", onvalue="!", offvalue="", variable=self.a, command=self.napis)
        a.pack(anchor="w")
        self.b = StringVar("")
        b = Checkbutton(self.zna1, text='"', onvalue='"', offvalue="", variable=self.b, command=self.napis)
        b.pack(anchor="w")
        self.c = StringVar("")
        c = Checkbutton(self.zna1, text="#", onvalue="#", offvalue="", variable=self.c, command=self.napis)
        c.pack(anchor="w")
        self.d = StringVar("")
        d = Checkbutton(self.zna1, text="$", onvalue="$", offvalue="", variable=self.d, command=self.napis)
        d.pack(anchor="w")
        self.e = StringVar("")
        e = Checkbutton(self.zna1, text="%", onvalue="%", offvalue="", variable=self.e, command=self.napis)
        e.pack(anchor="w")
        self.f = StringVar("")
        f = Checkbutton(self.zna1, text="&", onvalue="&", offvalue="", variable=self.f, command=self.napis)
        f.pack(anchor="w")
        self.g = StringVar("")
        g = Checkbutton(self.zna1, text="/", onvalue="/", offvalue="", variable=self.g, command=self.napis)
        g.pack(anchor="w")
        self.h = StringVar("")
        h = Checkbutton(self.zna1, text="(", onvalue="(", offvalue="", variable=self.h, command=self.napis)
        h.pack(anchor="w")
        self.i = StringVar("")
        i = Checkbutton(self.zna2, text=")", onvalue=")", offvalue="", variable=self.i, command=self.napis)
        i.pack(anchor="w")
        self.j = StringVar("")
        j = Checkbutton(self.zna2, text="=", onvalue="=", offvalue="", variable=self.j, command=self.napis)
        j.pack(anchor="w")
        self.k = StringVar("")
        k = Checkbutton(self.zna2, text="?", onvalue="?", offvalue="", variable=self.k, command=self.napis)
        k.pack(anchor="w")
        self.l = StringVar("")
        l = Checkbutton(self.zna2, text="+", onvalue="+", offvalue="", variable=self.l, command=self.napis)
        l.pack(anchor="w")
        self.m = StringVar("")
        m = Checkbutton(self.zna2, text="*", onvalue="*", offvalue="", variable=self.m, command=self.napis)
        m.pack(anchor="w")
        self.n = StringVar("")
        n = Checkbutton(self.zna2, text="'", onvalue="'", offvalue="", variable=self.n, command=self.napis)
        n.pack(anchor="w")
        self.o = StringVar("")
        o = Checkbutton(self.zna2, text="-", onvalue="-", offvalue="", variable=self.o, command=self.napis)
        o.pack(anchor="w")
        self.p = StringVar("")
        p = Checkbutton(self.zna3, text="_", onvalue="_", offvalue="", variable=self.p, command=self.napis)
        p.pack(anchor="w")
        self.r = StringVar("")
        r = Checkbutton(self.zna3, text="<", onvalue="<", offvalue="", variable=self.r, command=self.napis)
        r.pack(anchor="w")
        self.s = StringVar("")
        s = Checkbutton(self.zna3, text=">", onvalue=">", offvalue="", variable=self.s, command=self.napis)
        s.pack(anchor="w")
        self.t = StringVar("")
        t = Checkbutton(self.zna3, text=".", onvalue=".", offvalue="", variable=self.t, command=self.napis)
        t.pack(anchor="w")
        self.u = StringVar("")
        u = Checkbutton(self.zna3, text=":", onvalue=":", offvalue="", variable=self.u, command=self.napis)
        u.pack(anchor="w")
        self.v = StringVar("")
        v = Checkbutton(self.zna3, text=",", onvalue=",", offvalue="", variable=self.v, command=self.napis)
        v.pack(anchor="w")
        self.z = StringVar("")
        z = Checkbutton(self.zna3, text=";", onvalue=";", offvalue="", variable=self.z, command=self.napis)
        z.pack(anchor="w")

        # Izpis gesla

        self.konec = Frame(self.moz1)
        self.konec.pack(side="bottom")
        self.geslo1 = StringVar()
        konec0 = Button(self.konec, text="Daj mi geslo!", command=self.znano)

        self.konec1 = Entry(self.konec, textvariable=self.geslo1, justify=CENTER)
        self.konec1.pack(side="bottom")
        konec0.pack(side="bottom")
        # možnost 2
        self.moz2 = Frame(master)

        self.dolzina2 = StringVar()
        self.ges = Frame(self.moz2)
        self.ges.pack(fill="both")
        self.dolzina2.set("8")
        ges0 = Entry(self.ges, textvariable=self.dolzina2, justify=CENTER)
        ges0.bind("<Return>", self.nekaj)
        ges0.pack()
        self.geslo = StringVar()
        self.naklucno = Entry(self.ges, textvariable=self.geslo, justify=CENTER)
        ges1 = Button(self.ges, text="Lozinka!", command=self.nekaj)
        # self.geslo.set(nakljucna(int(self.dolzina2.get())))
        self.naklucno.pack(side="bottom", fill="x")
        ges1.pack()

        # izbira možnosti

        izb = LabelFrame(master, text="Vrsta gesla")
        izb.pack()
        self.prvo = StringVar()
        self.prvo.set("3")
        nic = Radiobutton(master, variable=self.prvo, value="3")
        izb0 = Radiobutton(izb, text="Deloma smiselno geslo", variable=self.prvo, value="0", command=self.odkrij)
        izb0.pack()
        izb1 = Radiobutton(izb, text="Nesmiselno geslo", variable=self.prvo, value="1", command=self.odkrij)
        izb1.pack()
        # ok = Button(izb, text="ok", command=self.odkrij)
        # ok.pack()


    def odkrij(self, asf=0):
        if self.prvo.get() == "0":
            self.moz1.pack()
            self.moz2.pack_forget()
            self.napaka.pack_forget()
        elif self.prvo.get() == "1":
            self.moz2.pack(fill="both")
            self.moz1.pack_forget()
            self.napaka.pack_forget()
        return None

    def nekaj(self, asf=0):
        try:
            self.geslo.set(nakljucna(int(self.dolzina2.get())))
            self.napaka.pack_forget()
        except:
            self.napaka.pack()
        return None


    def napis(self):
        v = self.a.get() + self.b.get() + self.c.get() + self.d.get() + self.e.get() + self.f.get() + self.g.get() + self.h.get() + self.i.get() + self.j.get() + self.k.get() + self.l.get() + self.m.get() + self.n.get() + self.o.get() + self.p.get() + self.r.get() + self.s.get() + self.t.get() + self.u.get() + self.v.get() + self.z.get()
        self.znaki.set(v)
        return None

    def znano(self, asfd=0):
        try:
            self.geslo1.set(geslo(list(self.znaki.get()), self.crk.get(), int(self.dolzina.get()), self.jezik.get()))
            self.konec1.pack()
            self.napaka.pack_forget()
        except:
            self.napaka.pack()

        return None


root = Tk()

root.title("Geslar")

aplikacija = Geslar(root)

root.mainloop()