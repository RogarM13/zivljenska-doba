__author__ = 'Gregor'

from os import urandom
from base64 import b64encode
from math import ceil
from random import randint, shuffle, choice
import re

import requests as req


def besedila(jezik, dolzina):
    ang = ["http://www.textfiles.com/food/acne1.txt","http://www.textfiles.com/games/arcade_c.hea",
           "http://www.textfiles.com/politics/993frmn.txt", "http://www.textfiles.com/holiday/xmas.stu"]

    slo = ["http://lit.ijs.si/samorast.html", "http://lit.ijs.si/podobeiz.html", "http://lit.ijs.si/cigler.html",
           "http://lit.ijs.si/hlapec.html", "http://lit.ijs.si/cvetjevj.html"]

    nem = ["http://www.gutenberg.org/cache/epub/2407/pg2407.txt", "http://www.gutenberg.org/cache/epub/2408/pg2408.txt",
           "http://www.gutenberg.org/cache/epub/37991/pg37991.txt"]
    z = " ([a-zA-Z]{}) ".format("{" + str(dolzina) + "}")
    if jezik == "1":
        a = len(ang)
        i = randint(0, a - 1)
        source = req.get(ang[i], auth=("user", "pass"))
        besedice = re.findall(z, source.text)
        return besedice
    elif jezik == "0":
        s = len(slo)
        i = randint(0, s - 1)
        source = req.get(slo[i], auth=("user", "pass"))
        besedice = re.findall(z, source.text)
        return besedice
    else:
        n = len(nem)
        i = randint(0, n - 1)
        source = req.get(nem[i], auth=("user", "pass"))
        besedice = re.findall(z, source.text)
        return besedice

def vm(bese):
    bese = list(bese)
    for i, j in enumerate(bese):
        if randint(0, 1) == 0:
            bese[i] = j.upper()
        else:
            bese[i] = j.lower()
    return "".join(bese)


def besede(dolzina, jezik):
    if dolzina > 20:
        dolzina = 20
    elif dolzina < 6:
        dolzina = 6
    k = []
    if dolzina == 6:
        k = [4]
    elif dolzina in [7, 8]:
        k = choice([[4], [5]])
    elif dolzina == 9:
        k = choice([[5], [6]])
    elif dolzina in [10, 11]:
        k = choice([[7], [8]])
    elif dolzina == 12:
        k = choice([[8], [7]])
    elif dolzina in [13, 14]:
        k = choice([[4, 4], [4, 5], [5, 5]])
    elif dolzina in [15, 16]:
        k = choice([[4, 5], [5, 5], [4, 6], [5, 6], [6, 6]])
    elif dolzina in [17, 18]:
        k = choice([[5, 6], [6, 6], [7, 5], [4, 8], [7, 6]])
    elif dolzina in [19, 20]:
        k = choice([[6, 6], [6, 7], [6, 8], [7, 7], [5, 8], [7, 8]])

    a = [choice(besedila(jezik, j)) for j in k]
    k = 0
    for i in a:
        k += len(i)
    return a, k



def geslo(znaki, velika, dolzina, jezik):
    z = [str(i) for i in range(0, 10)] + znaki
    k, l = besede(dolzina, jezik)

    dolzina = dolzina - l
    if velika == "0":
        for s, b in enumerate(k):
            k[s] = b.lower()
    elif velika == "1":
        for s, b in enumerate(k):
            k[s] = b.upper()
    elif velika == "2":
        for s, b in enumerate(k):
            k[s] = b.upper()
        t = randint(0, len(k) - 1)
        v = list(k[t])
        mala = randint(0, len(v) - 1)
        v[mala] = v[mala].lower()
        k[t] = "".join(v)
    elif velika == "3":
        for s, b in enumerate(k):
            k[s] = b.lower()
        t = randint(0, len(k) - 1)
        v = list(k[t])
        vela = randint(0, len(v) - 1)
        v[vela] = v[vela].upper()
        k[t] = "".join(v)
    else:
        for s, b in enumerate(k):
            k[s] = vm(b)
    skupaj = [z[randint(0, len(z) - 1)] for j in range(0, dolzina)] + k
    shuffle(skupaj)
    geslice = "".join(skupaj)
    return geslice



def nakljucna(dolzina):
    z = ceil(dolzina / 4)
    beseda = b64encode(urandom(3 * z)).decode('utf-8')
    return beseda[0:dolzina]
